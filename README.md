# std err-out driver for GitLab Runner demo

A didactic example to demonstrate how GitLab Runner's Custom Executor and
drivers work.

This is a [std err-out driver for GitLab Runner Custom
Executor](https://gitlab.com/ricardomendes/stderrout)'s companion repository,
intended to demonstrate how the driver works.

You can see the driver's friendly output
[here](https://gitlab.com/ricardomendes/stderrout-demo/-/jobs/633959828).
